﻿using APIVisitante.Bases;
using APIVisitante.DAO;
using Microsoft.AspNetCore.Mvc;
using Modelos.Bases;
using Modelos.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIVisitante.Controllers
{
    [Route("celulas")]
    public class CélulasController : Controller
    {
        private readonly BaseCélulasVisitanteRepositório _repositórioCélulas;
        private readonly BaseMonitoramentosRepositório _repositórioMonitoramentos;
        private readonly BaseAlertasVisitanteRepositório _repositórioAlertas;

        public CélulasController(BaseCélulasVisitanteRepositório repositórioCélulas, 
            BaseMonitoramentosRepositório repositórioMonitoramentos, BaseAlertasVisitanteRepositório repositórioAlertas)
        {
            _repositórioCélulas = repositórioCélulas;
            _repositórioMonitoramentos = repositórioMonitoramentos;
            _repositórioAlertas = repositórioAlertas;
        }

        [HttpGet]
        [Route("listar/{latitude}/{longitude}")]
        public async Task<ActionResult<List<SelecionarCélulaDAO>>> ListarCélulas(double latitude, double longitude)
        {
            return  _repositórioCélulas.RetornarListaCoordenadas(latitude, longitude);
            
        }

        [HttpGet]
        [Route("dados/{idCélula}")]
        public async Task<ActionResult<PáginaPrincipalDAO>> CarregarPáginaPrincipal(int idCélula)
        {
            var célula = _repositórioCélulas.RetornarPeloId(idCélula);
            var monitoramento = _repositórioMonitoramentos.RetornarÚltimo(m => m.Célula.Id == célula.Id);
            var alertas = _repositórioAlertas.RetornarÚltimosN(monitoramento.Id, 2);

            return new PáginaPrincipalDAO(célula, monitoramento, alertas);
        }
    }
}
