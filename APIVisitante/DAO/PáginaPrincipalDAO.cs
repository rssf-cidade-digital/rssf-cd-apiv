﻿using Modelos.Bibliotecas;
using Modelos.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIVisitante.DAO
{
    public class PáginaPrincipalDAO
    {
        public string EndereçoCélula { get; set; }
        public string ÚltimaLeituraCélula { get; set; }
        public int Temperatura { get; set; }
        public int Umidade { get; set; }
        public int UV { get; set; }
        public bool TemperaturaAlerta { get; set; }
        public bool UmidadeAlerta { get; set; }
        public bool UVAlerta { get; set; }
        public bool Gás { get; set; }
        public bool Fumaça { get; set; }
        public bool Fogo { get; set; }
        public List<string> Alertas { get; set; }

        public PáginaPrincipalDAO(CélulaModelo célula, MonitoramentoModelo monitoramento, List<AlertaModelo> alertas)
        {
            ObterDadosCélula(célula, monitoramento);
            ObterDadosMedidas(monitoramento);
            ObterDadosAlertas(monitoramento, alertas);
        }

        private void ObterDadosAlertas(MonitoramentoModelo monitoramento, List<AlertaModelo> alertas)
        {
            Alertas = new List<string>();

            var dataString = monitoramento.DataHora.ToString("dd/MM");
            foreach (var item in alertas)
            {
                Alertas.Add($".:{dataString}: {item.MensagemAlerta}");
            }
        }

        private void ObterDadosCélula(CélulaModelo célula, MonitoramentoModelo monitoramento)
        {
            EndereçoCélula = célula.Endereço;
            ÚltimaLeituraCélula = monitoramento.DataHora.ToString("dd/MM - HH:mm");
        }
        private void ObterDadosMedidas(MonitoramentoModelo monitoramento)
        {
            Temperatura = (int)Math.Round(monitoramento.Temperatura);
            Umidade = (int)Math.Round(monitoramento.Umidade);
            UV = (int)monitoramento.RadiaçãoUV;
            TemperaturaAlerta = CritériosAlertaBiblioteca.ChecarTemperatura(monitoramento.Temperatura);
            UmidadeAlerta = CritériosAlertaBiblioteca.ChecarUmidade(monitoramento.Temperatura);
            UVAlerta = CritériosAlertaBiblioteca.ChecarRadiaçãoUV(monitoramento.RadiaçãoUV);
            Gás = CritériosAlertaBiblioteca.ChecarNívelGás(monitoramento.GásTóxico);
            Fumaça = CritériosAlertaBiblioteca.ChecarNívelGás(monitoramento.Fumaça);
            Fogo = CritériosAlertaBiblioteca.ChecarIncêndio(monitoramento.Incêndio);
        }
    }
}
