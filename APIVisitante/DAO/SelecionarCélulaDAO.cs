﻿using Modelos.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIVisitante.DAO
{
    public class SelecionarCélulaDAO
    {
        public int Id { get; set; }
        public string Endereço { get; set; }
        public string Distância { get; set; }
        public SelecionarCélulaDAO(int iD, string endereço)
        {
            Id = iD;
            Endereço = endereço;
        }

        public SelecionarCélulaDAO(CélulaModelo célula, double latitude, double longitude) : this(célula.Id, célula.Endereço)
        {
            var distância = célula.CalcularDistância(latitude, longitude);
            if(distância > 1)
            {
                Distância = $"{String.Format("{0:0.00}", distância)} km";
            } 
            else
            {
                Distância = $"{String.Format("{0:0.00}", distância*1000)} m";
            }
        }
    }
}
