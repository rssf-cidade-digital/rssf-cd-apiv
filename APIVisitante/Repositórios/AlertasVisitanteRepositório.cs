﻿using APIVisitante.Bases;
using Modelos.Modelos;
using Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIVisitante.Repositórios
{
    public class AlertasVisitanteRepositório : BaseAlertasVisitanteRepositório
    {
        public AlertasVisitanteRepositório(AplicaçãoContext contexto) : base(contexto)
        {
        }

        public override List<AlertaModelo> RetornarÚltimosN(int idMonitoramento, int n)
        {
            var alertas = _baseDeDados.Where(a => a.Monitoramento.Id == idMonitoramento).OrderByDescending(a => a.Id).Take(n);
            return alertas.ToList();
        }
    }
}
