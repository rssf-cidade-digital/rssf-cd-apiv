﻿using APIVisitante.Bases;
using APIVisitante.DAO;
using Modelos;
using Modelos.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIVisitante.Repositórios
{
    public class CélulasVisitanteRepositório : BaseCélulasVisitanteRepositório
    {
        public CélulasVisitanteRepositório(AplicaçãoContext contexto) : base(contexto)
        {
        }

        public override List<SelecionarCélulaDAO> RetornarListaCoordenadas(double latitude, double longitude)
        {
            var retorno = new List<SelecionarCélulaDAO>();
            var células = base.RetornarLista().OrderBy(c => c.CalcularDistância(latitude, longitude)).ToList();

            foreach (var item in células)
            {
                retorno.Add(new SelecionarCélulaDAO(item, latitude, longitude));
            }

            return retorno;

        }
    }
}
