using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APIVisitante.Bases;
using APIVisitante.Repositórios;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Modelos;
using Modelos.Bases;
using Modelos.Repositórios;

namespace APIVisitante
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddDbContext<AplicaçăoContext>();

            services.AddTransient<BaseCélulasVisitanteRepositório, CélulasVisitanteRepositório>();
            services.AddTransient<BaseMonitoramentosRepositório, MonitoramentosRepositório>();
            services.AddTransient<BaseAlertasVisitanteRepositório, AlertasVisitanteRepositório>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
