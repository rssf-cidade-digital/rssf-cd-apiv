﻿using Modelos.Bases;
using Modelos.Modelos;
using Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIVisitante.Bases
{
    public abstract class BaseAlertasVisitanteRepositório : BaseRepositório<AlertaModelo>
    {
        public BaseAlertasVisitanteRepositório(AplicaçãoContext contexto) : base(contexto)
        {
        }

        public abstract List<AlertaModelo> RetornarÚltimosN(int idMonitoramento, int n);
    }
}
