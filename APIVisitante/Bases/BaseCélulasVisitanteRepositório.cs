﻿using Modelos.Bases;
using Modelos.Modelos;
using Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APIVisitante.DAO;

namespace APIVisitante.Bases
{
    public abstract class BaseCélulasVisitanteRepositório : BaseRepositório<CélulaModelo>
    {
        public BaseCélulasVisitanteRepositório(AplicaçãoContext contexto) : base(contexto)
        {
        }

        public abstract List<SelecionarCélulaDAO> RetornarListaCoordenadas(double latitude, double longitude);
    }
}
